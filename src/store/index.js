import { createStore } from 'vuex'

export default createStore({
  state: {
    menu: [
      "הדירה שלי ",
      "מצב חשבון ",
      "תיאום אדריכלי",
      "מחלקת הבדק",
      "חדשות ומבצעים",
      " צור קשר",
    ],
    showMenu: false,
  },
  mutations: {
    setShowMenu: (state, payload) => state.showMenu = payload,
  },
  getters: {
    getMenu: state => state.menu,
    getShowMenu: state => state.showMenu,
  },
  actions: {
    toggleMenuShow: ({ commit, getters }, show) => commit('setShowMenu', show ? show : !getters.getShowMenu),
  },
  modules: {
  }
})
