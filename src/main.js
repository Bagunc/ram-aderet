import { createApp } from 'vue'
import Primevue from 'primevue/config'

import configs from './configs'

import store from './store'
import router from './router'

import App from './App.vue'

import 'primeflex/primeflex.css'
import 'primevue/resources/primevue.min.css'

import './assets/scss/primevue-custom-theme.scss'
import './assets/scss/global.scss'

createApp(App)
  .use(store)
  .use(router)
  .use(Primevue, configs.primevue)
  .mount('#app')
